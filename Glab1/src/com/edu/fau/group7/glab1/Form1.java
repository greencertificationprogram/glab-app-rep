package com.edu.fau.group7.glab1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Form1 extends Activity {
	
	Button button1;
	Button button2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form1);
		addListenerOnButton();
	}
	
	
	protected void OnStart(){
		super.onStart();
	}
	
	public void addListenerOnButton()
	{
		final Context context = this;
		
		button1 = (Button) findViewById(R.id.button1);
		button2=(Button) findViewById(R.id.btnSubmit);
		button1.setOnClickListener(new OnClickListener(){
			@Override
				public void onClick(View arg0)
				{
					Intent intent= new Intent(context,MainActivity.class);
					startActivity(intent);
				}
		});
		
		button2.setOnClickListener(new OnClickListener(){
			@Override
				public void onClick(View arg0)
				{
					Intent intent= new Intent(context,Form2.class);
					startActivity(intent);
				}
		});
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
